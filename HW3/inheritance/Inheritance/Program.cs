﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ProgramConverter> myList = Data.CreateList();
            Data.TestInnerLogic(myList);
        }
    }

    interface IConvertable
    {
        string ConvertToCSharp(string line);
        string ConvertToVB(string line);
    }

    interface ICodeChecker
    {
        bool CheckCodeSyntax(string line, string language);
    }

    public static class Data
    {  
        public static List<ProgramConverter> CreateList()
        {
            List<ProgramConverter> myList = new List<ProgramConverter>();
            ProgramConverter myProgramConverter = new ProgramConverter();
            ProgramHelper myProgramHelper = new ProgramHelper();
            myList.Add(myProgramConverter);
            myList.Add(myProgramHelper);
            return myList;
        }

        public static void TestInnerLogic(List <ProgramConverter> myList)
        {
            foreach (ProgramConverter i in myList)
            {
                if (i is ICodeChecker)
                {
                    var tmp = (ProgramHelper)i;
                    if (tmp.CheckCodeSyntax("line", "CSharp"))
                    {
                        tmp.ConvertToVB("with ICodeChecker support");
                    }
                    else
                    {
                        tmp.ConvertToCSharp("with ICodeChecker support");
                    }

                }
                else
                {
                    i.ConvertToCSharp("without ICodeChecker support");
                    i.ConvertToVB("without ICodeChecker support");
                }
            }
            Console.ReadLine();
        }
    }

    public class ProgramHelper : ProgramConverter, ICodeChecker
    {
        public bool CheckCodeSyntax(string line, string language)
        {
            Console.WriteLine($"CheckCodeSyntax method {line} {language}");
            if (language == "CSharp")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
               
    }

    public class ProgramConverter : IConvertable
    {
        public string ConvertToCSharp(string line)
        {
            Console.WriteLine($"ConvertToCSharp method {line}");
            return line;
        }

        public string ConvertToVB(string line)
        {
            Console.WriteLine($"ConvertTOVB method {line}");
            return line;
        }
    }
}
