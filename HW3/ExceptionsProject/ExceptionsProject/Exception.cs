﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ExceptionsProject
{   
    [Serializable]
    public class NonConsistentMatrixException : Exception
    {
        public NonConsistentMatrixException() { }

        public NonConsistentMatrixException(string message) : base(message) { }

        public NonConsistentMatrixException(string message, Exception inner) : base(message, inner) { }

        public NonConsistentMatrixException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}
