﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExceptionsProject
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            M1_raws.Focus();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var Matrix1 = new Matrix(TextBlockParser.Parse(M1_raws.Text), TextBlockParser.Parse(M1_columns.Text));
                M1.Text += Environment.NewLine + Matrix1.ToString();
                M1.Visibility = System.Windows.Visibility.Visible;

                var Matrix2 = new Matrix(TextBlockParser.Parse(M2_raws.Text), TextBlockParser.Parse(M2_columns.Text));
                M2.Text += Environment.NewLine + Matrix2.ToString();
                M2.Visibility = Visibility.Visible;

                var Matrix3 = new Matrix(Matrix1, Matrix2);
                MR.Text += Environment.NewLine + Matrix3.ToString();
                MR.Visibility = Visibility.Visible;
            }
            catch (Exception err)
            {
                M1.Text = err.Message;
                M1.Visibility = Visibility.Visible;
            }
            finally
            {
                Create.Visibility = Visibility.Hidden;
            }

        }

    }
}
