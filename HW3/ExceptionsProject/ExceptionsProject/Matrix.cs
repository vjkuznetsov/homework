﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ExceptionsProject
{
    public class Matrix
    {
        private readonly int[,] data;
        private readonly int raws;
        private readonly int columns;
        static readonly Random rand = new Random();

        /// <summary>
        /// Создаёт матрицу заданного размера, заполненную случайными числами
        /// </summary>
        /// <param name="raws"></param>
        /// <param name="columns"></param>
        public Matrix(int raws, int columns)
        {
            if (raws <= 0 || columns <= 0)
            {
                throw new ArgumentException("Количества строк и столбцов должны быть натуральными числами");
            }
            else
            {
                this.raws = raws;
                this.columns = columns;
                this.data = new int[raws, columns];
                for (int i = 0; i < raws; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        this.data[i, j] = rand.Next(ConstantValue.MaxValueforMatrixElement);
                    }
                }
            }
        }

        /// <summary>
        /// Создаёт матрицу - произведение двух матриц (если они согласованные)
        /// </summary>
        /// <param name="M1"></param>
        /// <param name="M2"></param>
        public Matrix(Matrix M1, Matrix M2)
        {
            if (M1.Columns != M2.Raws)
            {
                throw new NonConsistentMatrixException(String.Format($"Количество столбцов первой матрицы {0} не равно количеству строк второй матрицы {1}", M1.Columns, M2.Raws));
            }
            else
            {
                int commonrange = M1.Columns;
                this.raws = M1.Raws;
                this.columns = M2.Columns;
                this.data = new int[raws, columns];
                for (int i = 0; i < raws; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        int counter = 0;
                        for (int k = 0; k < commonrange; k++)
                        {
                            counter += M1.Data[i, k] * M2.Data[k, j];
                        }
                        this.Data[i, j] = counter;
                    }
                }
            }
                
        }
        
        public override string ToString()
        {
            var tmp = new StringBuilder();
            for (int i = 0; i < Raws; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    tmp.AppendFormat("{0,4}", Data[i, j]);
                }
                tmp.Append(Environment.NewLine);
            }
            return tmp.ToString(); 
        } 

        public int Columns { get => columns; }
        public int Raws { get => raws; }
        public int[,] Data { get => data; }
    }
}
