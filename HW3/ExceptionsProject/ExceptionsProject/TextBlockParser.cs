﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionsProject
{
    public static class TextBlockParser
    {
        public static int Parse(string line)
        {
            if (!Int32.TryParse(line, out int results))
            {
                throw new FormatException(String.Format($"Некорректное значение поля {0}", line));
            }
            else
            {
                return results;
            }
        }

    }
}
