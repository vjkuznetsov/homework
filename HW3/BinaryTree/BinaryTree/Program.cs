﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            // К сожалению, задача не доделана до дедлайна и некоторая функциональность пока отсутствует
            // Полностью нет ui и обработки ошибок
            // Не реализованы конструкторы дерева с CompareObj и нет обратного перечислителя

            var myList = new List<int>();
            myList.Add(10);
            myList.Add(30);
            myList.Add(40);
            myList.Add(5);
            var myBT = new BinaryTree<int>(myList);
            myBT.InOrderTraversal();
            foreach (var t in myBT)
            {
                Console.WriteLine(t);
            }

            var myStudentList = new List<Student>();
            var Stu1 = new Student() { TestResult = 10, Name = "Vasya" };
            var Stu2 = new Student() { TestResult = 5, Name = "Kolya" };
            var Stu3 = new Student() { TestResult = 20, Name = "Roma" };
            myStudentList.Add(Stu1);
            myStudentList.Add(Stu2);
            myStudentList.Add(Stu3);
            var myBT2 = new BinaryTree<Student>(myStudentList);
            myBT2.InOrderTraversal();
            foreach (var t in myBT2)
            {
                Console.WriteLine(t);
            }

            Console.ReadLine();
        }
    }
}
