﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    public class Student : IComparable
    {
        private string name;
        private string testName;
        private byte testResult;
        private DateTime date;

        public string Name { get => name; set => name = value; }
        public string TestName { get => testName; set => testName = value; }
        public byte TestResult { get => testResult; set => testResult = value; }
        public DateTime Date { get => date; set => date = value; }

        public int CompareTo(object obj)
        {
            var item = (Student) obj;
            if (TestResult == item.TestResult)
            {
                return 0;
            }
            else if (TestResult > item.TestResult)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public override string ToString()
        {
            return $"Name: {Name}, TestName: {TestName}, TestResult: {TestResult}, DateTime: {Date}";
        }
    }
}
