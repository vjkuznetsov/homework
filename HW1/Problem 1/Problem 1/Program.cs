﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETStudent.HomeWork1
{
    class Program1
    {   /// <summary>
    /// Main method with check command line attributes and reset inputstream
    /// </summary>
    /// <param name="args"></param>
        static void Main(string[] args)
        {
            if ((args.Length == 2) && (args[0] == "<"))
            {
                try
                {
                    Console.SetIn(new StreamReader(args[1]));
                }
                catch (IOException e)
                {
                    TextWriter errorWriter = Console.Error;
                    errorWriter.WriteLine(e.Message);
                }
            }
            List<string> RawLines = getRawLinesfromConsole();
            RawLines = Class1.FormatLines(RawLines);
            PrintLinestoConsole(RawLines);
            Console.Read();
        }

        /// <summary>
        /// Method that reads data form the console
        /// </summary>
        /// <returns></returns>
        static private List<string> getRawLinesfromConsole()
        {
            string line;
            List<string> raw_lines = new List<string>();
            while (((line = Console.ReadLine()) != "") && (line != "null"))
            {
                raw_lines.Add(line);
            }
            return raw_lines;
        }

        /// <summary>
        /// Method that print dara to the console
        /// </summary>
        /// <param name="lines"></param>
        static private void PrintLinestoConsole(List<string> lines)
        {
            void Print(string s)
            {
                Console.WriteLine(s);
            }
            lines.ForEach(Print);
        }
               
    }
}