﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETStudent.HomeWork1
{
    public class Class1
    {   /// <summary>
    /// Method formats string data
    /// </summary>
    /// <param name="lines"></param>
    /// <returns></returns>
        static public List<string> FormatLines(List<string> lines)
        {
            List<string> res = new List<string>();
            foreach (string line in lines)
            {
                if (line.Split(',').Length == 2)
                {
                    res.Add("X: " + line.Split(',')[0] + " Y: " + line.Split(',')[1]);
                }
            }
            return res;
        }
    }
}
