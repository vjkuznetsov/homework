﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NETStudent.HomeWork1;
using Microsoft.Win32;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Button to close the main window
        /// </summary>
        private void escButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The method formats textdata from TextBox_input using FormatLines method from dll and
        /// prints results into TextBlock_output
        /// </summary>
        private void FormatButton_Click(object sender, RoutedEventArgs e)
        {
            TextBlock_output.Text = "";
            var Text = TextBox_input.Text;
            List<string> ListText = StringToListConverter(Text);
            ListText = Class1.FormatLines(ListText);
            foreach (string line in ListText)
            {
                TextBlock_output.Text += line + Environment.NewLine;
            }
           
        }

        /// <summary>
        /// OpenFileDialog using default openfileDialog by Microsoft.Win32
        /// </summary>
        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openfileDialog = new OpenFileDialog();
            openfileDialog.Filter = "Text files (*.txt)|*.txt";
            if (openfileDialog.ShowDialog() == true)
            {
                FileInfo fileinfo = new FileInfo(openfileDialog.FileName);
                using (StreamReader sr = fileinfo.OpenText())
                {
                    var Text = sr.ReadToEnd();
                    TextBox_input.Text = Text;
                }
            }
        }

        /// <summary>
        /// Method converts string data to List_string
        /// </summary>
        private List<string> StringToListConverter(string Text)
        {
            string[] arrayString = Text.Split('\n');
            List<string> tmp = new List<string>();
            foreach (string line in arrayString)
            {
                tmp.Add(line);
            }
            return tmp;
        }
    }
}
