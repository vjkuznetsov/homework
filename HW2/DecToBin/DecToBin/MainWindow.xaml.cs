﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace DecToBin
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainInputTextBox.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            try
            {
                var num = Int32.Parse(MainInputTextBox.Text, new CultureInfo("ru-RU"));
                if (num >= 0)
                {
                    OutPutTextBlock.Text = Convert.ToString(num, 2);
                }
                else
                {
                    OutPutTextBlock.Text = "Введено отрицательное значение";
                }
            }
            catch (Exception error)
            {
                OutPutTextBlock.Text = error.Message;
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
