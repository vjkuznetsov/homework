﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsulation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is demonstration of the methods of the class MailToData");
            Console.WriteLine("Create MailToData object with 2 attibutes: name and address in one line");
            Console.WriteLine("Please, enter name");
            MailToData mailtoData = new MailToData(Console.ReadLine());
            Console.WriteLine("Please, enter address in line");
            mailtoData.SetMailAdress(Console.ReadLine());
            if (mailtoData.SuccessfullParse)
            {
                Console.WriteLine("Correct address is " + mailtoData.GetMailAdress().ToString());
                Console.WriteLine("Name field: ");
                Console.WriteLine(mailtoData.Name);
                Console.WriteLine("City property: ");
                Console.WriteLine(mailtoData.City);
                Console.WriteLine("State property: ");
                Console.WriteLine(mailtoData.State);
                Console.WriteLine("Zip property: ");
                Console.WriteLine(mailtoData.Zip);
            }
            else
            {
                Console.WriteLine("Parse is unsuccessful, the properties are empty");
            }
            Console.ReadLine();
        }
    }

    public class MailToData
    {
        public string Name;
        private int zip;
        private string city;
        private string state;
        private bool IsSuccessfullParse;

        public MailToData(string Name = "", int _zip = 0, string _city = "default", string _state = "default")
        {
            this.Name = Name;
            Zip = _zip;
            City = _city;
            State = _state;
            IsSuccessfullParse = false;
        }

        public int Zip { get => zip; set => zip = value; }
        public string City { get => city; set => city = value; }
        public string State { get => state; set => state = value; }
        public bool SuccessfullParse { get => IsSuccessfullParse; }

        public string GetMailAdress()
        {
            return this.City + " " + this.State + " " + this.Zip.ToString();
        }

        public void SetMailAdress(string MailAdress)
        {
            MailAdress = MailAdress.Trim();
            if (MailAdress.Split(' ').Length < 3)
            {
                Console.WriteLine("Incorrect format: city, state, zip required ");
            }
            else
            {
                int result;
                if (!Int32.TryParse(MailAdress.Split(' ')[MailAdress.Split(' ').Length-1], out result))
                {
                    Console.WriteLine("Incorrect zip");
                }
                else
                {
                    this.zip = result;
                    int middle = (int)Math.Ceiling((double)((MailAdress.Split(' ').Length - 1) / 2));
                    string tmp1 = "";
                    for (int k = 0; k < middle; k++)
                    {
                        tmp1 += MailAdress.Split(' ')[k] + " ";
                    }
                    this.city = tmp1.Trim();
                    string tmp2 = "";
                    for (int k = middle; k < MailAdress.Split(' ').Length - 1; k++)
                    {
                        tmp2 += MailAdress.Split(' ')[k] + " ";
                    }
                    this.state = tmp2.Trim();
                    this.IsSuccessfullParse = true;

                }
            }
                       
        }

    }
}
