﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace Newtons_Method
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RadicandText.Focus();
        }

        public static double Pow(double BaseofPower, int exponent)
        {
            double powresult = 1;
            while (exponent > 0)
            {
                powresult *= BaseofPower;
                exponent--;
            }
            return powresult;
        }

        static double NewtonsNthRoot(double Radicand, double Exponent, double Epsilon)
        {
            var current_value = Radicand / Exponent;
            var next_value = (1 / Exponent) * ((Exponent - 1) * current_value + Radicand / Pow(current_value, (int)Exponent - 1));
            while (Math.Abs(next_value - current_value) > Epsilon)
            {
                current_value = next_value;
                next_value = (1 / Exponent) * ((Exponent - 1) * current_value + Radicand / Pow(current_value, (int)Exponent - 1));
            }
            return next_value;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CultureInfo myCultureInfo = new CultureInfo("ru-RU");
            Target_Value.Visibility = System.Windows.Visibility.Hidden;
            MathPow_Value.Visibility = System.Windows.Visibility.Hidden;
            Delta.Visibility = System.Windows.Visibility.Hidden;
            try
            {
                var Radicand = Double.Parse(RadicandText.Text, myCultureInfo);
                var Exponent = Double.Parse(ExponentText.Text, myCultureInfo);
                var Epsilon = Double.Parse(PrecisionText.Text, myCultureInfo);
                double MathPowValue = Math.Pow(Radicand, Math.Pow(Exponent, -1));
                if (Double.IsNaN(MathPowValue) == true || Math.Truncate(Exponent) != Exponent || Exponent <= 0)
                {
                    Target_Value.Visibility = System.Windows.Visibility.Visible;
                    Target_Value.Text = "Недопустимые входные значения";
                }
                else
                {
                    var NewtonsTargetValue = NewtonsNthRoot(Radicand, Exponent, Epsilon);
                    var DeltaValue = Math.Abs(NewtonsTargetValue - MathPowValue);
                    Target_Value.Text += NewtonsTargetValue.ToString();
                    MathPow_Value.Text += MathPowValue.ToString();
                    Delta.Text += DeltaValue.ToString();
                    Target_Value.Visibility = System.Windows.Visibility.Visible;
                    MathPow_Value.Visibility = System.Windows.Visibility.Visible;
                    Delta.Visibility = System.Windows.Visibility.Visible;
                }
                           
                
            }
            catch (Exception error) when (error is FormatException || error is OverflowException)
            {
                Target_Value.Visibility = System.Windows.Visibility.Visible;
                Target_Value.Text = error.Message;
            }
            
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
