﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timer
{
    class Program
    {
        static void Main(string[] args)
        {
            var mt = new MyTimer();
            mt.StartTimer += Show_Message;
            mt.HalfTimer += Show_Message;
            mt.FinishedTimer += Show_Message;
            mt.Start();
            Console.ReadLine();
        }

        private static void Show_Message(string message)
        {
            Console.WriteLine(message);
        }
    }
}
