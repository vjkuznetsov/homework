﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Timer
{
    public class MyTimer
    {
        public string Name { get; set; }
        public int Period { get; set; }

        public MyTimer(string Name = "default", int Period = 1000)
        {
            this.Name = Name;
            this.Period = Period;
        }

        public delegate void StartStatusHandler(string name);
        public delegate void HalfSecondRemainingHandler(string name);
        public delegate void FinishedStatusHandler(string name);

        public event StartStatusHandler StartTimer;
        public event HalfSecondRemainingHandler HalfTimer;
        public event FinishedStatusHandler FinishedTimer;
                
        public void Start()
        {
            StartTimer(Name);
            Thread.Sleep(Period - 500);
            HalfTimer(Name);
            Thread.Sleep(500);
            FinishedTimer(Name);
        }
    }
}
