﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
   

namespace TextEditor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            OpenFile.Focus();
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            var open = new OpenFileDialog() { CheckFileExists = true, Multiselect = false } ;
            open.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (open.ShowDialog() == true)
            {
                MainTextBox.Text = File.ReadAllText(open.FileName);
            }
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            var save = new SaveFileDialog();
            save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (save.ShowDialog() == true)
            {
                File.WriteAllText(save.FileName, MainTextBox.Text, Encoding.Default);
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            MainTextBox.Text = null;
        }
    }
}
