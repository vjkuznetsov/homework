﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var side_A = ui_io.InputSide();
                var side_B = ui_io.InputSide();
                var side_C = ui_io.InputSide();
                var Triangle = new TriangleClass(side_A, side_B, side_C);

                // Можно записать синтаксис как ниже, экономим три объявленные переменные
                // но мне кажется, что читаемость сильно падает
                //var Triangle = new TriangleClass(ui_io.InputSide(), ui_io.InputSide(), ui_io.InputSide());

                Console.WriteLine($"Периметр треугольника: {Triangle.Perimeter} ");
                Console.WriteLine($"Площадь треугольника: {Triangle.Square}");
                Console.Read();


            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
                Console.Read();
            }

        }
    }
}
