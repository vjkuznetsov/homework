﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    public class TriangleClass
    {
        public readonly int TriangeSideA;
        public readonly int TriangeSideB;
        public readonly int TriangeSideC;
        public readonly int Perimeter;
        public readonly double Square;

        public TriangleClass(int a, int b, int c)
        {
            if (IsSideLengthCorrect(a, b, c))
            {
                TriangeSideA = a;
                TriangeSideB = b;
                TriangeSideC = c;
                Perimeter = CalculatePerimeter();
                Square = CalculateSquare();
            }
            else
            {
                throw new ArgumentException("Треугольник с указанными длинами сторон не существует");
            }
                       
        }   
        
        /// <summary>
        /// Проверка "правила треугольника", любая из сторон меньше суммы двух других
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        private bool IsSideLengthCorrect(int a, int b, int c)
        {
            return (a < b + c) && (b < a + c) && (c < a + b);
        }

        /// <summary>
        /// Вычисление периметра
        /// </summary>
        /// <returns></returns>
        private int CalculatePerimeter()
        {
            return TriangeSideA + TriangeSideB + TriangeSideC;
        }

        /// <summary>
        /// Вычисление площади по формуле Герона
        /// Площадь треугольника равна корню произведения полупериметра и разностей полупериметра и каждой из сторон треугольника
        /// </summary>
        /// <returns></returns>
        private double CalculateSquare()
        {
            double _halfPerimeter = (double) Perimeter / 2;
            return Math.Pow((_halfPerimeter * (_halfPerimeter - TriangeSideA) *
                (_halfPerimeter - TriangeSideB) * (_halfPerimeter - TriangeSideC)), 0.5);
        }

      
    }
}
