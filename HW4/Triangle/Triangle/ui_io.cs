﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    static public class ui_io
    {
        /// <summary>
        /// Ввод длины стороны треугольника и проверка корректности этой длины
        /// </summary>
        /// <returns></returns>
        static public int InputSide()
        {
            Console.WriteLine("введите длину стороны треугольника");
            var _tmp = Console.ReadLine();
            if (Int32.TryParse(_tmp, out int num))
            {
                return num;
            }
            else
            {
                throw new FormatException($"Неверный формат {_tmp}");
            }
        }

    }
}
