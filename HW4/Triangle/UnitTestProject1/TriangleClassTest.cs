using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;   

namespace Triangle
{
    [TestClass]
    public class TriangleClassTest
    {
        [TestMethod]
        // �������� ������������, ���������� ��������
        public void CalculatePerimetr()
        {
            var triangle = new TriangleClass(3, 4, 5);
            Assert.AreEqual(12, triangle.Perimeter);
        }

        [TestMethod]
        // �������� ������������, ���������� �������
        public void CalculateSquare()
        {
            var triangle = new TriangleClass(3, 4, 5);
            Assert.AreEqual(6, triangle.Square);
        }

        // �� ����� ����, ������� ������������ (a + b) > c, (b + c) > a, (a + c) > b
        // ����������� ������� ����������� �� �������������� a, b, c � N, ��� ��� ����������� ���� � ������������� ����� ���������

        [TestMethod]
        // ���� �� ������ ����
        public void OneSideisZero()
        {
            try
            {
                var triangle = new TriangleClass(0, 2, 2);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }
        // ������� �������������
        [TestMethod]
        public void AllSideIsNegative()
        {
            try
            {
                var triangle = new TriangleClass(-1, -2, -3);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        // ��� ����� �� ������ ������ ������� ������������
        [TestMethod]
        public void TriangleRules_sideA()
        {
            try
            {
                var triangle = new TriangleClass(6, 3, 2);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        [TestMethod]
        public void TriangleRules_sideB()
        {
            try
            {
                var triangle = new TriangleClass(1, 7, 4);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        [TestMethod]
        public void TriangleRules_sideC()
        {
            try
            {
                var triangle = new TriangleClass(1, 2, 5);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        // ������������ ������� � ���������� �� ������� ������� ������ ��� �� �������
        // ���� � ������� ������ ����� � ������������, �� ����������� ��������� � ������� ����� ����� ����
        
         [TestMethod]
        public void ZeroExpressionUnderRadical()
        {
            var triangle = new TriangleClass(3, 8, 10);
            Assert.AreEqual(Math.Pow(98.4375,0.5), triangle.Square);
        }






    }
}
