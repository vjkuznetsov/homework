﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Test1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Findpath_emptyDirectory()
        {
            string path = @"D:\tmp\";
            Logger testLogger = new Logger();
            var t = FindWithTxtFiles.AllFoundFiles(testLogger, path);
            Assert.IsNull(t);
        }

        public void Findpath_OKDirectory()
        {
            string path = @"D:\tmp2\";
            Logger testLogger = new Logger();
            var t = FindWithTxtFiles.AllFoundFiles(testLogger, path);
            // юнит-тесты не успел :(

        }
    }
}
