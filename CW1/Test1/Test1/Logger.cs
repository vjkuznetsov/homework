﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Test1
{
    public interface ILogger
    {
        void ReporttoLog(string message);
        void PrintReport(string path);
    }

    public class Logger : ILogger
    {
        public List<string> data;

        void ILogger.ReporttoLog(string message)
        {
            data.Add(message);
        }

        public Logger()
        {
            data = new List<string>();
            data.Add($"Запуск логгера {DateTime.Now}");
        }

        void ILogger.PrintReport(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                StreamWriter sw = new StreamWriter(fs);
                foreach (var t in data)
                {
                    sw.WriteLine(t);
                }
            }
            
        }


    }
}
