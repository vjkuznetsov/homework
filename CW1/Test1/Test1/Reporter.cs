﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Test1
{
    [Serializable]
    public class Reporter
    {
        public string word;
        // Знаю, что интерфейс, возможно, был бы лучше, но время-время
        public List<DataFromFiletoReporter> data;
        
        // Почему-то атрибут Нонсериалайзд не работает тут. Почему?
        [NonSerialized] 
        private ILogger MyLogger;
        
        public Reporter(string Word, ILogger myLogger)
        {
            word = Word;
            data = new List<DataFromFiletoReporter>();
            MyLogger = myLogger;
            MyLogger.ReporttoLog($"{DateTime.Now} создан класс отчета");
        }

        // пустой конструктор для сериализатора
        public Reporter()
        {
        }

        public void Add(DataFromFiletoReporter data_file)
        {
            data.Add(data_file);
            MyLogger.ReporttoLog($"{DateTime.Now} в отчет добавлена информация о файле {data_file.path}");
        }

        public void PrintReport()
        {
            var t = data.OrderByDescending(x => x.counter);
            Console.WriteLine(word);
            foreach (var tmp in t)
            {
                Console.WriteLine(tmp);
            }
            MyLogger.ReporttoLog($"{DateTime.Now} отчет выведен на экран");
        }
    }

    [Serializable]
    public class DataFromFiletoReporter
    {
        public int counter;
        public string path;
        public long size;
        public DateTime DateChangeFile;

        public DataFromFiletoReporter(string Path, int Counter)
        {
            path = Path;
            counter = Counter;
            FileInfo file = new FileInfo(path);
            size = file.Length;
            DateChangeFile = file.LastWriteTimeUtc;
        }

        public override string ToString()
        {
            return $"path {path}, counter {counter}, length {size}, change date {DateChangeFile}";
        }

        public DataFromFiletoReporter()
        {

        }
    }

    

}
