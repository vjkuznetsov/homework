﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1
{
    public static class IEnumerableExtension
    {
        public static IEnumerable<string> WordSearch(this IEnumerable<string> worklist, string word)
        {
            return worklist.Where(x => x.Contains(word));
        }
    }
}
