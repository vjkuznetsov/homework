﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Test1
{
    public static class FindWithTxtFiles
    {
        public static string[] AllFoundFiles(ILogger myLogger, string path = "")
        {
            try
            {
                if (path == "")
                {
                    path = Directory.GetCurrentDirectory();
                    myLogger.ReporttoLog($"{DateTime.Now} Создается список файлов *.txt в директории по умолчанию {path}");
                }
                else
                {
                    myLogger.ReporttoLog($"{DateTime.Now} Создается список файлов *.txt в заданной директории {path}");
                }
                return Directory.GetFiles(path, "*.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        // Первая версия с заделом для задачи 2
        public static bool SearchWordInFile(string path, string word, ILogger MyLogger)
        {
            var targetLines = File.ReadAllLines(path)
                .Select((x, i) => new { Line = x, LineNumber = i })
                .Where(x => x.Line.Contains(word))
                .ToList();
            MyLogger.ReporttoLog($"{DateTime.Now} Произведен поиск подстроки {word} в файле {path}, найдено {targetLines.Count} вхождений");
            return (targetLines.Count > 0);
        }

        // В задаче 3 задел пригодился
        public static int SearchandCountWordInFile(string path, string word, ILogger MyLogger)
        {
            var targetLines = File.ReadAllLines(path)
                .Select((x, i) => new { Line = x, LineNumber = i })
                .Where(x => x.Line.Contains(word))
                .ToList();
            MyLogger.ReporttoLog($"{DateTime.Now} Произведен поиск подстроки {word} в файле {path}, найдено {targetLines.Count} вхождений");
            return targetLines.Count;
        }

        public static IEnumerable<string> ReturnPathFilethatContainsWord(string[] pathArray, string word, ILogger MyLogger)
        {
            foreach (var path in pathArray)
            {
                if (SearchWordInFile(path, word, MyLogger))
                {
                    yield return path;
                }
            }
            yield break;

        }



    }
}
