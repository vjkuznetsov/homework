﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Мини-отладку/тесты начал писать здесь
            // Времени перенести все в тесты не осталось

            // 

            // Задание 1
            //List<string> test = new List<string>() { "vasya", "petya", "vova", "vava" };
            //foreach (var word_ in test.WordSearch("va"))
            //{
            //    Console.WriteLine(word_);
            //}
            //Console.ReadLine();

            // Задание 2
            // Совсем-совсем нет времени на тестовый файл, знаю что так делать не надо
            // Тестик на создание дерева файлов
            var testLogger = new Logger();
            string testpath = "";
            var k = FindWithTxtFiles.AllFoundFiles(testLogger, testpath);
            foreach (var path in FindWithTxtFiles.AllFoundFiles(testLogger, testpath))
            {
                Console.WriteLine(path);
            }
            Console.ReadLine();

            //// Тестик на правильный поиск строки в файле
            //var path_ = @"C:\Users\net\Documents\HW\CW1\Test1\Test1\bin\Debug\TestFile.txt";
            //var word = "vas";
            //Console.WriteLine(FindWithTxtFiles.SearchWordInFile(path_, word));
            //Console.ReadLine();

            //// тест yeild/return
            //var patharray = FindWithTxtFiles.AllFoundFiles();
            //foreach (var path in FindWithTxtFiles.ReturnPathFilethatContainsWord(patharray, "vas"))
            //{
            //    Console.WriteLine(path);
            //}

            // Задание 3, 4
            var Logger1 = new Logger();
            var myLogger = (ILogger)Logger1;
            var patharray = FindWithTxtFiles.AllFoundFiles(myLogger);
            var word = "vas";
            var myReporter = new Reporter(word, myLogger);
            foreach (var path in FindWithTxtFiles.ReturnPathFilethatContainsWord(patharray, word, myLogger))
            {
                var i = FindWithTxtFiles.SearchandCountWordInFile(path, word, myLogger);
                if (i > 0)
                {
                    var data = new DataFromFiletoReporter(path, i);
                    myReporter.Add(data);
                }
            }

            myReporter.PrintReport();
            Console.ReadLine();

            // задание 5. Сериализатор
            Serialization.SerializationMethod(myReporter, myLogger);

            // Задание 4
            myLogger.PrintReport("logger.txt");
        }
    }
}
