﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Test1
{
    public static class Serialization
    {
        public static void SerializationMethod(Reporter myReporter, ILogger MyLogger)
        {
            using (FileStream fs = new FileStream("repo.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(Reporter));
                formatter.Serialize(fs, myReporter);
                fs.Flush();
                MyLogger.ReporttoLog($"{DateTime.Now} произведена сериализация отчета {nameof(myReporter)}");
            }
              
        }
    }
}
