﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TODO.DAL.Entities;
using TODO.Common.Types;

namespace TODO.DAL.Context
{
	class TODOInitializer : DropCreateDatabaseAlways<TODOContext>
	{
		protected override void Seed(TODOContext context)
		{
			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number one",
				Description = "Some description for task one",
				DateCreated = DateTime.Now.AddDays(-1),
				DateDue = DateTime.Now.AddDays(5),
				State = TaskStateType.Cancelled,
				Direction = TaskDirectionType.Outcome
			});

			context.TaskSet.Add(new TaskEntity
			{
				Title = "Task number two",
				Description = "Description for task two",
				DateCreated = DateTime.Now,
				DateDue = DateTime.Now.AddDays(1),
				State = TaskStateType.InProgress,
				Direction = TaskDirectionType.Income
			});

			base.Seed(context);
		}
	}
}
