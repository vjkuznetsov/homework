﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Config;
using TODO.DAL.Entities;

namespace TODO.DAL.Context
{
	/// <summary>
	/// Контекст БД
	/// </summary>
	public class TODOContext : DbContext
	{
		/// <summary>
		/// DbSet Задачи
		/// </summary>
		public DbSet<TaskEntity> TaskSet { get; set; }

		/// <summary>
		/// Dbset Контрагенты
		/// </summary>
		public DbSet<CounteragentEntity> CounteragentSet { get; set; }

		/// <summary>
		/// Инициализатор контекста БД
		/// </summary>
		/// <param name="connectionString">Строка подключения к БД.</param>
		public TODOContext(string connectionString = ConnectionString.LocalMain) : base(connectionString)
		{

		}

		/// <summary>
		/// Конструктор БД и сущностей
		/// </summary>
		/// <param name="modelBuilder"></param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			Database.SetInitializer(new TODOInitializer());
			base.OnModelCreating(modelBuilder);
		}
	}
}
