﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью Задача.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class TaskRepository : IRepository<TaskDTO>
	{
		private TODOContext _context;

		public TaskRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект TaskDTO</param>
		public void Create(TaskDTO item)
		{
			var entity = Mapper.Map<TaskEntity>(item);
			_context.TaskSet.Add(entity);
			
			// сохраняем в БД, чтобы получить ID нового объекта
			_context.SaveChanges();
			item.ID = entity.ID;
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект TaskDTO</returns>
		public TaskDTO Read(int id)
		{
			var entity = _context.TaskSet.Find(id);
			var dto = Mapper.Map<TaskDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов TaskDTO</returns>
		public IEnumerable<TaskDTO> Read(Expression<Func<TaskDTO, bool>> query)
		{
			var dtos = _context.TaskSet.ProjectTo<TaskDTO>().Where(query);
			return dtos;
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов TaskDTO</returns>
		public IEnumerable<TaskDTO> ReadAll()
		{
			var dtos = Mapper.Map<IEnumerable<TaskEntity>, IEnumerable<TaskDTO>>(_context.TaskSet);
			return dtos;
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект TaskDTO</param>
		public void Update(TaskDTO item)
		{
			var newEntity = Mapper.Map<TaskEntity>(item);
			var oldEntity= _context.TaskSet.Find(item.ID);

			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			_context.Entry(oldEntity).State = EntityState.Modified;
			_context.SaveChanges();
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			_context.Entry(new TaskEntity { ID = id }).State = EntityState.Deleted;
			_context.SaveChanges();
		}
	}
}
