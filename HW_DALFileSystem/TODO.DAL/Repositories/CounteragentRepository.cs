﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL.Repositories
{
	/// <summary>
	/// Репозиторий для работы с сущностью Задача.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class CounteragentRepository : IRepository<CounteragentDTO>
	{
		private TODOContext _context;

		public CounteragentRepository(TODOContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект CounteragentDTO</param>
		public void Create(CounteragentDTO item)
		{
			var entity = Mapper.Map<CounteragentEntity>(item);
			_context.CounteragentSet.Add(entity);
			
			// сохраняем в БД, чтобы получить ID нового объекта
			_context.SaveChanges();
			item.ID = entity.ID;
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект CounteragentDTO</returns>
		public CounteragentDTO Read(int id)
		{
			var entity = _context.CounteragentSet.Find(id);
			var dto = Mapper.Map<CounteragentDTO>(entity);
			return dto;
		}

		/// <summary>
		/// Чтение списка объектов по заданному linq-выражению
		/// </summary>
		/// <param name="query">linq-выражение</param>
		/// <returns>Спискок объектов CounteragentDTO</returns>
		public IEnumerable<CounteragentDTO> Read(Expression<Func<CounteragentDTO, bool>> query)
		{
			var dtos = _context.CounteragentSet.ProjectTo<CounteragentDTO>().Where(query);
			return dtos;
		}

		/// <summary>
		/// Чтение всех объектов
		/// </summary>
		/// <returns>Спискок объектов CounteragentDTO</returns>
		public IEnumerable<CounteragentDTO> ReadAll()
		{
			var dtos = Mapper.Map<IEnumerable<CounteragentEntity>, IEnumerable<CounteragentDTO>>(_context.CounteragentSet);
			return dtos;
		}

		/// <summary>
		/// Обновление объекта
		/// </summary>
		/// <param name="item">Объект CounteragentDTO</param>
		public void Update(CounteragentDTO item)
		{
			var newEntity = Mapper.Map<CounteragentEntity>(item);
			var oldEntity= _context.CounteragentSet.Find(item.ID);

			_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
			_context.Entry(oldEntity).State = EntityState.Modified;
			_context.SaveChanges();
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		public void Delete(int id)
		{
			_context.Entry(new CounteragentEntity { ID = id }).State = EntityState.Deleted;
			_context.SaveChanges();
		}
	}
}
