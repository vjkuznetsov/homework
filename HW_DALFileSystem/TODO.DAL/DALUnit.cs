﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Config;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.Context;
using TODO.DAL.Entities;

namespace TODO.DAL
{
	/// <summary>
	/// Класс, реализующий паттерн Unit Of Work для БД
	/// На вход и выход всех репозиториев передаются DTO-модели
	/// Работа с Entity-моделями происходит только в репозиториях
	/// </summary>
	public class DALUnit
	{
		/// <summary>
		/// Контекст БД
		/// </summary>
		public TODOContext _context { get; set; }

		/// <summary>
		/// Репозиторий Задачи
		/// </summary>
		public IRepository<TaskDTO> Tasks { get; set; }

		/// <summary>
		/// Репозиторий Контрагенты
		/// </summary>
		public IRepository<CounteragentDTO> Counteragents { get; set; }

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="connectionString">
		/// Строка подключения к БД. По умолчанию используется главная база.
		/// Параметр нужен для того, чтобы в тестах задавать тестовую базу
		/// </param>
		public DALUnit(IRepository<TaskDTO> tasks, IRepository<CounteragentDTO> counteragents)
		{
			_context = new TODOContext();

			MapperInitialize();

			Tasks = tasks;
			Counteragents = counteragents;
		}

		/// <summary>
		/// Сохранение всех изменений в БД
		/// </summary>
		public void SaveChanges()
		{
			_context.SaveChanges();
		}

		/// <summary>
		/// Инициализация маппера.
		/// У слоя DAL свой маппер, т.к. Entity-модели должны быть видны только в этом слое.
		/// Наружу DAL отдает DTO-модели
		/// </summary>
		private void MapperInitialize()
		{
			Mapper.Initialize(x => x.CreateMap<TaskEntity, TaskDTO>().ReverseMap());
		}
	}
}
