﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.DAL;

namespace TODO.BAL.Services
{
	/// <summary>
	/// Класс сервиса для Задач.
	/// Обработка исключений из слоя DAL происходит здесь
	/// </summary>
	public class TaskService : ITaskService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		public string ErrorMessage { get { return _errorMessage; } }
		string _errorMessage;

		/// <summary>
		/// Объект доступа к слою DAL
		/// </summary>
		DALUnit _dal;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="dal">Объект доступа к слою DAL</param>
		public TaskService(DALUnit dal)
		{
			_dal = dal;
		}

		/// <summary>
		/// Создание задачи
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		public bool Create(TaskDTO item)
		{
			try
			{
				item.DateCreated = DateTime.Now;
				_dal.Tasks.Create(item);
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Прочитить все активные задачи.
		/// Активной считается задача со статусом "В процессе" или "Просрочена"
		/// </summary>
		/// <returns>Список активных задач</returns>
		public IEnumerable<TaskDTO> ReadActive()
		{
			try
			{
				return _dal.Tasks.Read(x => x.State == TaskStateType.InProgress || x.State == TaskStateType.Expired);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочитать все архивные задачи.
		/// Архивной считается задача со статусом "Завершена" или "Отменена"
		/// </summary>
		/// <returns>Список архивных задач</returns>
		public IEnumerable<TaskDTO> ReadArchive()
		{
			try
			{
				return _dal.Tasks.Read(x => x.State == TaskStateType.Completed || x.State == TaskStateType.Cancelled);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочтитать список задач по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список задач</returns>
		public IEnumerable<TaskDTO> ReadCustom(Expression<Func<TaskDTO, bool>> query)
		{
			try
			{
				return _dal.Tasks.Read(query);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Обновление информации о задаче.
		/// Обновляемые поля:
		/// Title (название),
		/// Description (описание),
		/// DateDue (дата завершения),
		/// Direction (направление).
		///
		/// Игнорируемые поля:
		/// DateCreated (дата создания),
		/// State (состояние)
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		public bool Update(TaskDTO item)
		{
			try
			{
				_dal.Tasks.Update(item);
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}

		/// <summary>
		/// Установка состояния. Используется в методах реализации
		/// этого интерфейса (ITaskService)
		/// </summary>
		/// <param name="taskID">ID Задачи</param>
		/// <param name="newState">Новое сосотояние</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		private bool SetStateHelper(int taskID, TaskStateType newState)
		{
			try
			{
				var item = _dal.Tasks.Read(taskID);
				item.State = newState;
				_dal.Tasks.Update(item);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Установить состояние задачи "Завершена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		public bool SetComplete(int id)
		{
			return SetStateHelper(id, TaskStateType.Completed);
		}

		/// <summary>
		/// Установить состояние задачи "Отменена"
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		public bool SetCancel(int id)
		{
			return SetStateHelper(id, TaskStateType.Cancelled);
		}

		/// <summary>
		/// Установить состояние задачи "Просрочена".
		/// Метод должен вызыватся в наблюдателе, который переодически
		/// проверяет текущую дату и устанавливает статус "Просрочена"
		/// к соответствующим задачам
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Установка статуса прошла успешно - true, иначе false</returns>
		public bool SetExpired(int id)
		{
			return SetStateHelper(id, TaskStateType.Expired);
		}

		/// <summary>
		/// Повторение ранее завершенной или отменённой задачи.
		/// После выполнения создаётся новая задача, старая остаётся неизменной.
		/// </summary>
		/// <param name="item">Объект задачи TaskDTO</param>
		/// <returns></returns>
		public bool Repeat(TaskDTO item)
		{
			var newItem = new TaskDTO();
			newItem.Title = item.Title;
			newItem.Description = item.Description;
			newItem.DateDue = item.DateDue;

			return Create(newItem);
		}

		/// <summary>
		/// Полное удаление задачи из базы данных
		/// </summary>
		/// <param name="id">ID задачи</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		public bool Delete(int id)
		{
			try
			{
				_dal.Tasks.Delete(id);
				_dal.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
	}
}
