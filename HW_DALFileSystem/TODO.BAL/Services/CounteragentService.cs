﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.DAL;

namespace TODO.BAL.Services
{
	/// <summary>
	/// Класс сервиса для Контрагентов.
	/// Обработка исключений из слоя DAL происходит здесь
	/// </summary>
	public class CounteragentService : ICounteragentService
	{
		/// <summary>
		/// Сообщение об ошибке. Используется для отображения
		/// пользователю в MessageBox
		/// </summary>
		public string ErrorMessage { get { return _errorMessage; } }
		string _errorMessage;

		/// <summary>
		/// Объект доступа к слою DAL
		/// </summary>
		DALUnit _dal;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="dal">Объект доступа к слою DAL</param>
		public CounteragentService(DALUnit dal)
		{
			_dal = dal;
		}

		/// <summary>
		/// Создание контрагента
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Создание прошло успешно - true, иначе false</returns>
		public bool Create(CounteragentDTO item)
		{
			try
			{
				_dal.Counteragents.Create(item);
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
		
		/// <summary>
		/// Прочтитать список всех контрагентов.
		/// </summary>
		/// <returns>Список контрагентов</returns>
		public IEnumerable<CounteragentDTO> ReadAll()
		{
			try
			{
				return _dal.Counteragents.ReadAll();
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Прочтитать список контрагентов по произвольному условию.
		/// Используется для фильтрации данных
		/// </summary>
		/// <param name="query">linq-вырвжение</param>
		/// <returns>Список контрагентов</returns>
		public IEnumerable<CounteragentDTO> ReadCustom(Expression<Func<CounteragentDTO, bool>> query)
		{
			try
			{
				return _dal.Counteragents.Read(query);
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return null;
			}
		}

		/// <summary>
		/// Обновление информации о контрагенте
		/// </summary>
		/// <param name="item">Объект контрагента CounteragentDTO</param>
		/// <returns>Обновление прошло успешно - true, иначе false</returns>
		public bool Update(CounteragentDTO item)
		{
			try
			{
				_dal.Counteragents.Update(item);
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
		
		/// <summary>
		/// Полное удаление контрагента из базы данных
		/// </summary>
		/// <param name="id">ID контрагента</param>
		/// <returns>Удаление прошло успешно - true, иначе false</returns>
		public bool Delete(int id)
		{
			try
			{
				_dal.Counteragents.Delete(id);
				_dal.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = ExceptionHelper.AddInner(ex);
				return false;
			}
		}
	}
}
