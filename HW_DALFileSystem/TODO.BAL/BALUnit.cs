﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.BAL.Services;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL;

namespace TODO.BAL
{
	/// <summary>
	/// Класс бизнес-логики приложения, паттерн Unit Of Work
	/// </summary>
	public class BALUnit : IBALUnit
	{
		/// <summary>
		/// Объект доступа к слою DAL
		/// </summary>
		DALUnit _dal;

		/// <summary>
		/// Сервис Задачи
		/// </summary>
		public ITaskService Tasks { get; set; }

		/// <summary>
		/// Сервис Контрагенты
		/// </summary>
		public ICounteragentService Counteragents { get; set; }


		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="dal">Объект доступа к слою DAL</param>
		public BALUnit(DALUnit dal, ITaskService tasks, ICounteragentService counteragents)
		{
			_dal = dal;

			Tasks = tasks;
			Counteragents = counteragents;
		}
	}
}
