﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Config;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.DAL.FS;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace TODO.DAL.ConsoleUI
{
	/// <summary>
	/// Стартовый класс с методом Main
	/// </summary>
	class Program
	{
		
		/// <summary>
		/// Точка старта приложения
		/// </summary>
		/// <param name="args">аргументы командной строки</param>
		static void Main(string[] args)
		{
			Console.WriteLine("begin");
            var dal = new DALUnit(new TaskRepository());

            

			CreateTask();

			ReadAllTasks();

			Console.WriteLine("\nend");
			Console.ReadKey();
		}

		/// <summary>
		/// Инициализация контейнера Unity
		/// </summary>
		
		/// <summary>
		/// Отображение исключения и завершение работы приложения
		/// </summary>
		/// <param name="ex"></param>
		static void ShowException(Exception ex)
		{
			Console.WriteLine(ex.Message);
			if (ex.InnerException != null)
			{
				Console.WriteLine("\nInner exception:");
				Console.WriteLine(ex.InnerException.Message);
				Console.ReadKey();
				Environment.Exit(0);
			}
		}

		/// <summary>
		/// Проверка: создание объекта задача в БД
		/// </summary>
		static void CreateTask()
		{
			var rnd = new Random().Next(100, 500);

			var dto = new TaskDTO();
			dto.Title = "Task N " + rnd;
			dto.Description = $"Description for task {rnd}";
			dto.DateCreated = DateTime.Now;
			dto.DateDue = DateTime.Now.AddDays(5);

			try
			{
                TaskRepository taskRepo = new TaskRepository();
                taskRepo.Create(dto);
				
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		/// <summary>
		/// Проверка: чтение списка задач из БД
		/// </summary>
		static void ReadAllTasks()
		{
			try
			{
                TaskRepository taskRepo = new TaskRepository();
                var item = taskRepo.Read(2);
				Console.WriteLine($"{item.ID} {item.Title} \t {item.Description}");
				Console.WriteLine($"{item.DateCreated}\n{item.DateDue}\n{item.State}\n{item.Direction}\n");
				
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}
	}
}
