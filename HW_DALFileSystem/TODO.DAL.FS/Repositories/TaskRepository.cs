﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Interfaces;

namespace TODO.DAL.FS
{
	/// <summary>
	/// Репозиторий для работы с сущностью Задача.
	/// Обработку исключений выполнять в вышестоящих классах
	/// </summary>
	public class TaskRepository : IRepository<TaskDTO>
	{
        /// <summary>
        /// Десериализация хранилища из файла в оперативную память в объект IList
        /// </summary>
        /// <returns></returns>
        private List<TaskEntity> Initialize()
        {
            List<TaskEntity> newList = Serialization.DeserializationMethod(typeof(TaskEntity));
            if (newList == null)
                return new List<TaskEntity>();
            else return newList;

        }
        /// <summary>
        /// Сериализация и сохранение коллекции объектов в файл на диске
        /// </summary>
        /// <param name="entityList"></param>
        private void SaveChanges(List<TaskEntity> entityList)
        {
            Serialization.SerializationMethod(typeof(TaskEntity), entityList);
        }

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <param name="item">Объект TaskDTO</param>
		public void Create(TaskDTO item)
		{
			var entity = Mapper.Map<TaskEntity>(item);
            var workList = (List<TaskEntity>)Initialize();
            if ((int)workList.Count > 0)
            {
                entity.ID = workList.Select(x => x.ID).Max() + 1;
            }
            else
            {
                entity.ID = 0;
            }
            workList.Add(entity);
                
            SaveChanges(workList);                   
		}

		/// <summary>
		/// Чтение объекта
		/// </summary>
		/// <param name="id">Идентификатор объекта</param>
		/// <returns>Объект TaskDTO</returns>
		public TaskDTO Read(int id)
		{
            var _workList = Initialize();
            var entity = (TaskEntity) _workList.Where(item => item.ID == id).First();
            var dto = Mapper.Map<TaskDTO>(entity);
			return dto;
		}

        ///// <summary>
        ///// Чтение списка объектов по заданному linq-выражению
        ///// </summary>
        ///// <param name="query">linq-выражение</param>
        ///// <returns>Спискок объектов TaskDTO</returns>
        //public IEnumerable<TaskDTO> Read(Expression<Func<TaskDTO, bool>> query)
        //{
        //          var _workList = Initialize();
        //          var entity = _workList.Where(query);
        //	var dtos = _context.TaskSet.ProjectTo<TaskDTO>().Where(query);
        //	return dtos;
        //}

        /// <summary>
        /// Чтение всех объектов
        /// </summary>
        /// <returns>Спискок объектов TaskDTO</returns>
        public IEnumerable<TaskDTO> ReadAll()
        {
            var _workList = Initialize();
            var dtos = Mapper.Map<IEnumerable<TaskEntity>, IEnumerable<TaskDTO>>(_workList);
            return dtos;
        }

        ///// <summary>
        ///// Обновление объекта
        ///// </summary>
        ///// <param name="item">Объект TaskDTO</param>
        //public void Update(TaskDTO item)
        //{
        //	var newEntity = Mapper.Map<TaskEntity>(item);
        //	var oldEntity= _context.TaskSet.Find(item.ID);

        //	_context.Entry(oldEntity).CurrentValues.SetValues(newEntity);
        //	_context.Entry(oldEntity).State = EntityState.Modified;
        //	_context.SaveChanges();
        //}

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        public void Delete(int id)
        {
            var _workList = Initialize();
            var entity = (TaskEntity)_workList.Where(item => item.ID == id).First();
            _workList.Remove(entity);
            entity.State = Common.Types.TaskStateType.Cancelled;
            _workList.Add(entity);
            SaveChanges(_workList);
        }
    }
}
