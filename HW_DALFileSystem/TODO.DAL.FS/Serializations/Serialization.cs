﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace TODO.DAL.FS
{
    public static class Serialization
    {
        public static bool SerializationMethod(Type type, List<TaskEntity> entityList)
        {
            var path = CheckFilePathAttribute(type);
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<TaskEntity>));
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, entityList);
                    fs.Flush();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private static string CheckFilePathAttribute(Type type)
        {
            object[] attrs = type.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                if (attr is FilePathAttribute myattr)
                {
                    return myattr.FilePath;
                }
            }
            return "default.xml"; ;
        }

        public static List<TaskEntity> DeserializationMethod(Type type)
        {
            var path = CheckFilePathAttribute(type);
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<TaskEntity>));
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    return (List<TaskEntity>)formatter.Deserialize(fs);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
