﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.DAL.FS
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FilePathAttribute : System.Attribute 
    {
        public string FilePath { get; set; }

        public FilePathAttribute(string path = "default.xml")
        {
            FilePath = path;
        }
    }
}
