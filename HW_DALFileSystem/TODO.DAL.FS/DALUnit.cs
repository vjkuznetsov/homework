﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using TODO.Common.DTO;
using TODO.Common.Interfaces;

namespace TODO.DAL.FS
{
    public class DALUnit
    {
        /// <summary>
        /// Репозиторий Задачи
        /// </summary>
        public IRepository<TaskDTO> Tasks { get; set; }

        /// <summary>
        /// Репозиторий Контрагенты
        /// </summary>
        public IRepository<CounteragentDTO> Counteragents { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="connectionString">
        /// Строка подключения к БД. По умолчанию используется главная база.
        /// Параметр нужен для того, чтобы в тестах задавать тестовую базу
        /// </param>
        public DALUnit(IRepository<TaskDTO> tasks)
        {
            MapperInitialize();

            Tasks = tasks;
        }

        /// <summary>
        /// Сохранение всех изменений в БД
        /// </summary>
        public void SaveChanges()
        {
           
        }

        /// <summary>
        /// Инициализация маппера.
        /// У слоя DAL свой маппер, т.к. Entity-модели должны быть видны только в этом слое.
        /// Наружу DAL отдает DTO-модели
        /// </summary>
        private void MapperInitialize()
        {
            Mapper.Initialize(x => x.CreateMap<TaskEntity, TaskDTO>().ReverseMap());
        }
    }
}
