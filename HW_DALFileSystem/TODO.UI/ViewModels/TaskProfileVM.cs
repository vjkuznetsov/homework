﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;
using TODO.UI.Infrastructure;
using Unity;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для окна профиля задачи TaskProfileView
	/// </summary>
	public class TaskProfileVM : PropertyChangedBase
	{
		/// <summary>
		/// Объект доступа к бизнес-логики приложения
		/// </summary>
		IBALUnit _bal;

		/// <summary>
		/// Признак создания новой задачи.
		/// Если в конструктор передается null - будет создана новая задача
		/// </summary>
		private bool NewTaskRequested;

		/// <summary>
		/// Сообщение об ошибке
		/// </summary>
		private string _errorMessage;
		public string ErrorMessage
		{
			get { return _errorMessage; }
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(null, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		//
		// Свойства для отображения модели TaskDTO
		//

		/// <summary>
		/// Идентификатор задачи
		/// </summary>
		private int _id;
		public int ID
		{
			get { return _id; }
			set { _id = value; NotifyPropertyChanged("ID"); }
		}

		/// <summary>
		/// Название задачи задачи
		/// </summary>
		private string _title;
		public string Title
		{
			get { return _title; }
			set { _title = value; NotifyPropertyChanged("Title"); }
		}

		/// <summary>
		/// Описание задачи задачи
		/// </summary>
		private string _description;
		public string Description
		{
			get { return _description; }
			set { _description = value; NotifyPropertyChanged("Description"); }
		}

		/// <summary>
		/// Дата создания задачи задачи
		/// </summary>
		private DateTime _dateCreated;
		public DateTime DateCreated
		{
			get { return _dateCreated; }
			set { _dateCreated = value; NotifyPropertyChanged("DateCreated"); }
		}

		/// <summary>
		/// Дата выполнения задачи задачи
		/// </summary>
		private DateTime _dateDue;
		public DateTime DateDue
		{
			get { return _dateDue; }
			set { _dateDue = value; NotifyPropertyChanged("DateDue"); }
		}

		/// <summary>
		/// Признак бессрочного выполнения задачи
		/// </summary>
		private bool _dateDueUnlimit;
		public bool DateDueUnlimit
		{
			get { return _dateDueUnlimit; }
			set { _dateDueUnlimit = value; NotifyPropertyChanged("DateDueUnlimit"); }
		}

		/// <summary>
		/// Направление задачи (входящая / исходящая)
		/// </summary>
		private TaskDirectionType _direction;
		public TaskDirectionType Direction
		{
			get { return _direction; }
			set { _direction = value; NotifyPropertyChanged("Direction"); }
		}

		//
		// События управления состоянием View
		//

		/// <summary>
		/// Событие подтверждения изменений, для команды OKCommand
		/// </summary>
		public event EventHandler OKEvent;

		/// <summary>
		/// Событие отмены изменений, для команды CancelCommand
		/// </summary>
		public event EventHandler CancelEvent;

		/// <summary>
		/// Событие возникновения ошибки
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		//
		// Команды
		//

		/// <summary>
		/// Команда подтверждения и внесения изменений в БД
		/// </summary>
		public RelayCommand OKCommand { get; private set; }

		/// <summary>
		/// Команда отмены изменений
		/// </summary>
		public RelayCommand CancelCommand { get; private set; }

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="task">Задача для отображения. В случае null - будет создана новая задача</param>
		public TaskProfileVM(IBALUnit bal, TaskDTO task)
		{
			// Инициализация объекта доступа к слою BAL

			_bal = bal;

			// Инициализация команд

			OKCommand = new RelayCommand(OK);
			CancelCommand = new RelayCommand(Cancel);

			// Установка признака создания новой задачи

			if (task == null)
			{
				// для новых задач время выполнения по умолчанию - 1 день

				DateCreated = DateTime.Now;
				DateDue = DateCreated.AddDays(1);
				NewTaskRequested = true;
				return;
			}

			// Заполнение окна данными задачи

			ID = task.ID;
			Title = task.Title;
			Description = task.Description;

			if (task.DateDue == null)
			{

			}
			else
			{

			}

			DateCreated = task.DateCreated;
			DateDue = task.DateDue;
			DateDueUnlimit = false;
			Direction = task.Direction;
		}

		/// <summary>
		/// Метод подтверждения и внесения изменений в БД
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void OK(object parameter)
		{
			// Валидация введённых данных

			if (string.IsNullOrWhiteSpace(Title) == true)
			{
				ErrorMessage = "Заполните название задачи";
				return;
			}

			// Создание объекта DTO для отправки в бизнес-слой BAL

			var task = new TaskDTO();
			task.ID = ID;
			task.Title = Title;
			task.Description = Description;
			task.DateCreated = DateCreated;
			task.DateDue = DateDue;
			task.Direction = Direction;

			// Создание или обновление объекта в БД

			bool result;

			if (NewTaskRequested == true)
			{
				result = _bal.Tasks.Create(task);
			}
			else
			{
				result = _bal.Tasks.Update(task);
			}

			// Неуспешное выполнение команды

			if (result == false)
			{
				ErrorMessage = _bal.Tasks.ErrorMessage;
				return;
			}

			// Успешное выполнение команды, вызвать OKEvent

			OKEvent?.Invoke(null, null);
		}

		/// <summary>
		/// Метод отмены изменений
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void Cancel(object parameter)
		{
			CancelEvent?.Invoke(null, null);
		}
	}
}
