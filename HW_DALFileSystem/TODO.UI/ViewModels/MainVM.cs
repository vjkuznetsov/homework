﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.DTO;
using TODO.Common.Helpers;
using TODO.Common.Interfaces.Services;
using TODO.Common.Types;
using TODO.UI.Helpers;
using TODO.UI.Infrastructure;
using TODO.UI.Views;
using Unity;

namespace TODO.UI.ViewModels
{
	/// <summary>
	/// ViewModel для главного окна MainView
	/// </summary>
	public class MainVM : PropertyChangedBase
	{
		/// <summary>
		/// Объект доступа к бизнес-логики приложения
		/// </summary>
		IBALUnit _bal;

		/// <summary>
		/// Список задач. Испльзуется для отображения в ListView главного окна
		/// </summary>
		public ICollection<TaskDTO> TaskList { get; set; }

		/// <summary>
		/// Выбранная задача
		/// </summary>
		public TaskDTO SelectedTask { get; set; }


		/// <summary>
		/// Заголовок текущего списка задач (активные / архивные)
		/// </summary>
		private string _taskListCaption;
		public string TaskListCaption
		{
			get { return _taskListCaption; }
			set
			{
				_taskListCaption = value;
				NotifyPropertyChanged("TaskListCaption");
			}
		}

		/// <summary>
		/// Сообщение об ошибке
		/// </summary>
		private string _errorMessage;
		public string ErrorMessage
		{
			get { return _errorMessage; }
			private set
			{
				_errorMessage = value;
				NotifyPropertyChanged(ErrorMessage);
				ErrorEvent?.Invoke(this, new StringMessageEventArgs { Message = ErrorMessage });
			}
		}

		/// <summary>
		/// Событие возникновения ошибки
		/// </summary>
		public event StringMessageHandler ErrorEvent;

		/// <summary>
		/// Команда создания задачи
		/// </summary>
		public RelayCommand CreateTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода профиля задачи
		/// </summary>
		public RelayCommand ShowTaskDetailsCommand { get; private set; }

		/// <summary>
		/// Команда завершения задачи
		/// </summary>
		public RelayCommand CompleteTaskCommand { get; private set; }

		/// <summary>
		/// Команда отмены задачи
		/// </summary>
		public RelayCommand CancelTaskCommand { get; private set; }

		/// <summary>
		/// Команда вывода активных задач
		/// </summary>
		public RelayCommand ShowActiveTasksCommand { get; private set; }

		/// <summary>
		/// Команда вывода архивных задач
		/// </summary>
		public RelayCommand ShowArchiveTasksCommand { get; private set; }

		//
		// Состояние интерфейса
		//


		/// <summary>
		/// Признак отображения активного списка задач.
		/// Используется для задания состояния элементов View
		/// </summary>
		private bool _isActiveTaskList;
		private bool IsActiveTaskList
		{
			get { return _isActiveTaskList; }
			set
			{
				_isActiveTaskList = value;
				UpdateViewControls();
			}
		}

		/// <summary>
		/// Признак доступности кнопки "Завершить задачу"
		/// </summary>
		private bool _isEnabledButtonCompleteTask { get; set; }
		public bool IsEnabledButtonCompleteTask
		{
			get { return _isEnabledButtonCompleteTask; }
			set { _isEnabledButtonCompleteTask = value; NotifyPropertyChanged("IsEnabledButtonCompleteTask"); }
		}

		/// <summary>
		/// Признак доступности кнопки "Отменить задачу"
		/// </summary>
		private bool _isEnabledButtonCancelTask { get; set; }
		public bool IsEnabledButtonCancelTask
		{
			get { return _isEnabledButtonCancelTask; }
			set { _isEnabledButtonCancelTask = value; NotifyPropertyChanged("IsEnabledButtonCancelTask"); }
		}

		/// <summary>
		/// Конструктор
		/// </summary>
		public MainVM(IBALUnit bal)
		{
			// Инициализация объекта доступа к слою BAL

			_bal = bal;

			// Инициализация команд

			CreateTaskCommand = new RelayCommand(CreateTask);
			ShowTaskDetailsCommand = new RelayCommand(ShowTaskDetails);
			CompleteTaskCommand = new RelayCommand(CompleteTask);
			CancelTaskCommand = new RelayCommand(CancelTask);
			ShowActiveTasksCommand = new RelayCommand(ShowActiveTasks);
			ShowArchiveTasksCommand = new RelayCommand(ShowArchiveTasks);

			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод создания задачи
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void CreateTask(object parameter)
		{
			var taskProfile = new TaskProfileView(_bal);
			if(taskProfile.ShowDialog() == false)
			{
				return;
			}
			
			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод создания задачи. Аналогичен методу CreateTask,
		/// только в качестве параметра для TaskProfileView передается SelectedTask
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void ShowTaskDetails(object parameter)
		{
			if(SelectedTask == null)
			{
				return;
			}

			var taskProfile = new TaskProfileView(_bal, SelectedTask);
			if(taskProfile.ShowDialog() == false)
			{
				return;
			}
			
			// TODO сделать добавление нового элемента в локальный список без вызова этого метода,
			// TODO сейчас заново перечитывается весь список из БД
			ShowActiveTasks(null);
		}

		/// <summary>
		/// Метод завершения задачи
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void CompleteTask(object parameter)
		{
			if (SelectedTask == null)
			{
				return;
			}

			var result = _bal.Tasks.SetComplete(SelectedTask.ID);
			if (result == false)
			{
				ErrorMessage = _bal.Tasks.ErrorMessage;
				return;
			}

			// Удаляем задачу из локального списка, чтобы не перечитывать список из БД

			TaskList.Remove(SelectedTask); 
			NotifyPropertyChanged("TaskList");
		}

		/// <summary>
		/// Метод отмены задачи
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void CancelTask(object parameter)
		{
			if (SelectedTask == null)
			{
				return;
			}

			var result = _bal.Tasks.SetCancel(SelectedTask.ID);
			if (result == false)
			{
				ErrorMessage = _bal.Tasks.ErrorMessage;
				return;
			}

			// Удаляем задачу из локального списка, чтобы не перечитывать список из БД

			TaskList.Remove(SelectedTask);
			NotifyPropertyChanged("TaskList");
		}

		/// <summary>
		/// Метод вывода активных задач
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void ShowActiveTasks(object parameter)
		{
			var result = _bal.Tasks.ReadActive();
			if (result == null)
			{
				ErrorMessage = _bal.Tasks.ErrorMessage;
				return;
			}

			TaskList = new ObservableCollection<TaskDTO>(result);
			NotifyPropertyChanged("TaskList");

			IsActiveTaskList = true;
		}

		/// <summary>
		/// Метод вывода архивных задач
		/// </summary>
		/// <param name="parameter">Команда для RelayCommand</param>
		private void ShowArchiveTasks(object parameter)
		{
			var result = _bal.Tasks.ReadArchive();
			if (result == null)
			{
				ErrorMessage = _bal.Tasks.ErrorMessage;
				return;
			}

			TaskList = new ObservableCollection<TaskDTO>(result);
			NotifyPropertyChanged("TaskList");

			IsActiveTaskList = false;
		}

		/// <summary>
		/// Обновление состояния элементов View
		/// </summary>
		private void UpdateViewControls()
		{
			if (IsActiveTaskList == true)
			{
				TaskListCaption = "Активные задачи";
				IsEnabledButtonCompleteTask = true;
				IsEnabledButtonCancelTask = true;
			}

			else
			{
				TaskListCaption = "Архив задач";
				IsEnabledButtonCompleteTask = false;
				IsEnabledButtonCancelTask = false;
			}
		}
	}
}
