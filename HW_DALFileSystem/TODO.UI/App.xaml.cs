﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TODO.Common.Config;
using TODO.Common.Interfaces.Services;
using TODO.UI.Infrastructure;
using Unity;

namespace TODO.UI
{
	/// <summary>
	/// Класс старта приложения
	/// </summary>
	public partial class App : Application
	{
		/// <summary>
		/// Метод старта приложения
		/// </summary>
		/// <param name="e">аргументы страта приложения</param>
		protected override void OnStartup(StartupEventArgs e)
		{
			DependencyFactory.Initialize();

			var bal = DependencyFactory.Container.Resolve<IBALUnit>();

			var mainView = new Views.MainView(bal);
			mainView.Show();

			base.OnStartup(e);
		}
	}
}
