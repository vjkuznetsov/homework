﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.BAL;
using TODO.BAL.Services;
using TODO.Common.Config;
using TODO.Common.DTO;
using TODO.Common.Interfaces;
using TODO.Common.Interfaces.Services;
using TODO.DAL;
using TODO.DAL.Context;
using TODO.DAL.Repositories;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace TODO.UI.Infrastructure
{
	/// <summary>
	/// Класс фабрика контейнера Unity
	/// </summary>
	public static class DependencyFactory
	{
		/// <summary>
		/// Объект контейнера Unity
		/// </summary>
		public static IUnityContainer Container;

		/// <summary>
		/// Инициализация контейнера Unity
		/// </summary>
		public static void Initialize()
		{
			Container = new UnityContainer();

			// Передача строки подключения в TODOContext минуя DALUnity

			Container.RegisterInstance("ConnectionString", ConnectionString.LocalMain, new ContainerControlledLifetimeManager());
			var cs = new InjectionConstructor(new ResolvedParameter<string>("ConnectionString"));
			Container.RegisterType<TODOContext>(cs);

			// Регистрация DALUnit - класс доступа к слою DAL

			Container.RegisterType<DALUnit>(new ContainerControlledLifetimeManager());

			// Регистрация репозиториев

			Container.RegisterType<IRepository<TaskDTO>, TaskRepository>();
			Container.RegisterType<IRepository<CounteragentDTO>, CounteragentRepository>();

			// Регистрация сервисов

			Container.RegisterType<IBALUnit, BALUnit>(new ContainerControlledLifetimeManager());
			Container.RegisterType<ITaskService, TaskService>();
			Container.RegisterType<ICounteragentService, CounteragentService>();
		}
	}
}
