﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TODO.Common.DTO;
using TODO.Common.Interfaces.Services;

namespace TODO.UI.Views
{
    /// <summary>
    /// Тест создания и чтения задач
    /// </summary>
    public partial class TestView : Window
    {
		/// <summary>
		/// Объект доступа к бизнес-логики приложения
		/// </summary>
		IBALUnit _bal;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="bal">Объект доступа к бизнес-логики приложения</param>
        public TestView(IBALUnit bal)
        {
            InitializeComponent();

			_bal = bal;

			CreateTask();
			CreateTask();
			CreateTask();
			ReadActive();
        }

		/// <summary>
		/// Проверка: создание объекта задача в БД через слой BAL
		/// </summary>
		void CreateTask()
		{
			var rnd = new Random(DateTime.Now.Millisecond).Next(100, 500);

			var dto = new TaskDTO();
			dto.Title = "New task from UI " + rnd;
			dto.Description = "Some description " + rnd;
			dto.DateDue = DateTime.Now.AddDays(rnd);

			var result = _bal.Tasks.Create(dto);

			if (result == false)
			{
				labTest.Content = _bal.Tasks.ErrorMessage;
				return;
			}
		}

		/// <summary>
		/// Проверка: чтение списка задач из БД через слой BAL
		/// </summary>
		void ReadActive()
		{
			var result = _bal.Tasks.ReadActive();
			if(result == null)
			{
				labTest.Content = _bal.Tasks.ErrorMessage;
				return;
			}

			var sb = new StringBuilder();
			foreach(var item in result)
			{
				sb.Append($"{item.ID} {item.Title} \t {item.Description}\n");
				sb.Append($"{item.DateCreated}\n{item.DateDue}\n{item.State}\n{item.Direction}\n\n");
			}

			labTest.Content = sb;
		}
    }
}
