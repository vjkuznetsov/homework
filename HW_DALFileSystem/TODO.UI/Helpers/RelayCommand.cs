﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TODO.UI.Helpers
{
	/// <summary>
	/// Обработчик команд
	/// </summary>
	public class RelayCommand : ICommand
	{
		/// <summary>
		/// Команда
		/// </summary>
		private Action<object> _execute;

		/// <summary>
		/// Возможность выполнения
		/// </summary>
		private Func<object, bool> _canExecute;

		/// <summary>
		/// Событие вызывается при изменении возможности выполнения команды
		/// </summary>
		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="execute">Команда</param>
		/// <param name="canExecute">Возможность выполнения</param>
		public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
		{
			_execute = execute;
			_canExecute = canExecute;
		}

		/// <summary>
		/// Проверка возможности выполнения команды
		/// </summary>
		/// <param name="parameter">Параметр команды</param>
		/// <returns>В случае возможности выполнения - true, иначе false</returns>
		public bool CanExecute(object parameter)
		{
			return _canExecute == null || _canExecute(parameter);
		}

		/// <summary>
		/// Выполнение команды
		/// </summary>
		/// <param name="parameter">Параметр команды</param>
		public void Execute(object parameter)
		{
			_execute(parameter);
		}
	}
}
