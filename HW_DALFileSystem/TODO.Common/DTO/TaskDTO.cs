﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO.Common.Types;

namespace TODO.Common.DTO
{
	/// <summary>
	/// DTO Задача
	/// </summary>
	public class TaskDTO
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название задачи
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Описание задачи
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Дата создания задачи
		/// </summary>
		public DateTime DateCreated { get; set; }

		/// <summary>
		/// Планируемая дата выполнения задачи
		/// </summary>
		public DateTime DateDue { get; set; }

		/// <summary>
		/// Статус задачи
		/// </summary>
		public TaskStateType State { get; set; }

		/// <summary>
		/// Тип задачи: Исходящая / Входящая
		/// </summary>
		public TaskDirectionType Direction { get; set; }
	}
}
