﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Config
{
	/// <summary>
	/// Строки подключения к БД
	/// </summary>
	public static class ConnectionString
	{
		// Для отдельной установки MSSQL Server

		/// <summary>
		/// Главная база
		/// </summary>
		public const string Main = @"Data Source=localhost; Initial Catalog=TODO; Integrated Security=True;";

		/// <summary>
		/// База для тестов
		/// </summary>
		public const string ForTests = @"Data Source=localhost; Initial Catalog=TODOForTests; Integrated Security=True;";

		// Для MSSQL из комплекта Visual Studio. Используется при разработке на машине без установленного MSSQL

		/// <summary>
		/// Главная база
		/// </summary>
		public const string LocalMain = @"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog=TODO; Integrated Security=True;";

		/// <summary>
		/// База для тестов
		/// </summary>
		public const string LocalForTests = @"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog=TODOForTests; Integrated Security=True;";
	}
}
