﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Interfaces.Services
{
	/// <summary>
	/// Интерфейс бизнес-логики приложения
	/// </summary>
	public interface IBALUnit
	{
		/// <summary>
		/// Интерфейс сервиса Задачи
		/// </summary>
		ITaskService Tasks { get; set; }
	}
}
