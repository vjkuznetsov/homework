﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Helpers
{
	/// <summary>
	/// Класс-хелпер обработки события изменения состояния объекта.
	/// Может использоваться во ViewModel для уведомления View об изменениях
	/// </summary>
	public class PropertyChangedBase : INotifyPropertyChanged
	{
		/// <summary>
		/// Событие при изменении объекта
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Вызов события PropertyChanged
		/// </summary>
		/// <param name="property">Имя вызывающего объекта</param>
		protected void NotifyPropertyChanged([CallerMemberName]string property = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
		}
	}
}
