﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Types
{
	/// <summary>
	/// Статус задачи
	/// </summary>
	public enum TaskStateType
	{
		/// <summary>
		/// Задача в процессе выполнения
		/// </summary>
		[Description("В процессе")]
		InProgress,

		/// <summary>
		/// Задача завершена
		/// </summary>
		[Description("Завершена")]
		Completed,

		/// <summary>
		/// Задача отменена
		/// </summary>
		[Description("Отменена")]
		Cancelled,

		/// <summary>
		/// Задача просрочена
		/// </summary>
		[Description("Просрочена")]
		Expired
	}
}
