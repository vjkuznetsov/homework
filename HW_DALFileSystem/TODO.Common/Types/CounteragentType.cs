﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Types
{
	/// <summary>
	/// Тип контрагента
	/// </summary>
	public enum CounteragentType
	{
		/// <summary>
		/// Физическое лицо
		/// </summary>
		IndividualPerson,

		/// <summary>
		/// Юридическое личо
		/// </summary>
		Legal
	}
}
