﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO.Common.Types
{
	/// <summary>
	/// Тип задачи: Исходящая / Входящая.
	/// Исходящая задача - задача, которую должен выполнить создатель
	/// Входящая задача - задача, которую кто-то должен выполнить для создателя
	/// </summary>
	public enum TaskDirectionType
	{
		/// <summary>
		/// Тип "Исходящая задача"
		/// </summary>
		[Description("Исходящая")]
		Outcome,

		/// Тип "Входящая задача"
		[Description("Входящая")]
		Income
	}
}
